<?php

App::import('Vendor', 'HTML2PDF.HTML2PDF', array(
	'file' => 'html2pdf' . DS . 'html2pdf.class.php'
));

class Pdf extends HTML2PDF {

/**
 * Default PDF generation settings
 *
 * @var array
 */
	public $settings = array(
		'orientation' => 'P',
		'format' => 'A4',
		'language' => 'en',
		'unicode' => true,
		'encoding' => 'UTF-8',
		'margin' => array(
			'left' => 5,
			'top' => 5,
			'right' => 5,
			'bottom' => 8
		)
	);

/**
 * Constructor
 *
 * @param array $settings
 */
	public function __construct($settings = array()) {
		$settings = Hash::merge($this->settings, $settings);
		if (is_integer($settings['margin'])) {
			$settings['margin'] = array(
				'left' => $settings['margin'],
				'top' => $settings['margin'],
				'right' => $settings['margin'],
				'bottom' => $settings['margin']
			);
		}
		parent::__construct(
			$settings['orientation'],
			$settings['format'],
			$settings['language'],
			$settings['unicode'],
			$settings['encoding'],
			array(
				$settings['margin']['left'],
				$settings['margin']['top'],
				$settings['margin']['right'],
				$settings['margin']['bottom']
			)
		);
	}

}
