<?php

App::uses('View', 'View');
App::uses('Pdf', 'HTML2PDF.Utility');

class PdfView extends View {

/**
 * The subdirectory. PDF views are always in pdf.
 *
 * @var string
 */
	public $subDir = 'pdf';

/**
 * Constructor
 *
 * @param Controller $controller
 */
	public function __construct(Controller $controller = null) {
		parent::__construct($controller);
		if (isset($controller->response) && $controller->response instanceof CakeResponse) {
			$controller->response->type('pdf');
			$this->layoutPath = 'pdf';
		}
	}

	public function render($view = null, $layout = null) {
		$settings = array();
		if (isset($this->viewVars['_pdf'])) {
			$settings = Hash::merge($settings, $this->viewVars['_pdf']);
			unset($this->viewVars['_pdf']);
		}
		if ($view !== false && $this->_getViewFileName($view)) {
			$content = parent::render($view, $layout);
			$Pdf = new Pdf($settings);
			$Pdf->WriteHTML($content);
			return $Pdf->Output();
		}
	}

}
